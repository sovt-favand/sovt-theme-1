<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/assets/js/foundation.min.js', array( 'jquery' ), '6.0', true );
    
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
    
    // add Slick for the carousel
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/vendor/owl-carousel/owl.carousel.min.js', array('jquery'), '', true );
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/vendor/owl-carousel/owl.carousel.css', array(), '', 'all' );
    wp_enqueue_style( 'owl-carousel-theme', get_template_directory_uri() . '/vendor/owl-carousel/owl.theme.css', array('owl-carousel'), '', 'all' );
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);