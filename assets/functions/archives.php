<?php
function joints_archive_query_adjustments( $query ) {
    
    // doesn't apply if we're in the admin or if this isn't the main loop
    if ( is_admin() || ! $query->is_main_query() )
        return;

    // if this is the sports cpt archive, show all sports, order by sport name
    if ( is_post_type_archive( 'sport' ) ) {
        // Display 50 posts for a custom post type called 'movie'
        $query->set( 'posts_per_page', 200 );
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        return;
    }
    
    // if this is the programs cpt archive, show all programs
    if ( is_post_type_archive( 'local' ) ) {
        // Display 50 posts for a custom post type called 'movie'
        $query->set( 'posts_per_page', 500 );
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        return;
    }
}
add_action( 'pre_get_posts', 'joints_archive_query_adjustments', 1 );