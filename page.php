<?php get_header(); ?>
			
<div id="content">

	<div id="inner-content" class="row">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
            <?php 
            // if there is a parent page, show the side navigation area
            $main_column_class = '';
            
            if ($post->post_parent): 
                $main_column_class = 'column small-12 medium-8 large-9';
            
                ?>
                
                
                <div class="sibling-menu small-12 medium-4 large-3 column">
                    
                    
                    <?php if( $post->post_parent ) : // If there is a post parent, show it ?>
                        <a class="previous-page-link" href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>
                    <?php else: ?>
                        <?php echo $post->post_title; ?>
                    <?php endif; ?>
                  
                    <ul>   
                        <?php // show sibling pages, and child pages under this page 
                        $siblings = get_pages('child_of='.get_post($post->post_parent)->ID.'&parent='.get_post($post->post_parent)->ID);
                        foreach($siblings as $sibling){
                        
                            if ($sibling->ID == $post->ID) { // if this is the current page ?>
                                <li><?php echo $post->post_title; ?>
                                    <?php
                                    $children = get_pages('child_of='.$post->ID.'&parent='.$post->ID);
                                    if (count($children) !=0): ?>
                                        <ul>    
                                        <?php foreach($children as $child) { ?>
                                            <li><a href="<?php echo $child->guid; ?>"><?php echo $child->post_title; ?></a></li>
                                        <?php } ?>
                                        
                                        
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            
                            <?php } else { ?>
                                <li><a href="<?php echo $sibling->guid; ?>"><?php echo $sibling->post_title; ?></a></li>
                            <?php }
                            
                        } ?>
                    </ul>
                       
                </div>
            
            <?php
            endif;
            ?>

            <main id="main" class="column <?php echo $main_column_class; ?>" role="main">
            
                
                    <?php get_template_part( 'parts/loop', get_post_type() ); ?>
                    
                

            </main> <!-- end #main -->

        <?php endwhile; else : ?>
		
            <main class="column" role="main">
			    <?php get_template_part( 'parts/content', 'missing' ); ?>
            </main>
            
		<?php endif; ?>


		<?php /* get_sidebar(); */?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>			    
