<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <a class="previous-page-link" href="<?php echo get_post_type_archive_link('local'); ?>">In Your Town</a>
    						
	<header class="article-header">	
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
	
    <?php get_template_part( 'parts/content', 'contacts' ); ?>
    <?php get_template_part( 'parts/content', 'sports' ); ?>
    
    <section class="entry-content" itemprop="articleBody">
		<?php the_content(); ?>
	</section> <!-- end article section -->
    
    <?php get_template_part( 'parts/content', 'sponsors' ); ?>
						
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
									
	<?php comments_template(); ?>	
													
</article> <!-- end article -->