<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <?php 
        $hclass = has_post_thumbnail() ? 'has-background' : '';
        $hstyle = has_post_thumbnail() ? "style=\"background-image: url('" . wp_get_attachment_url( get_post_thumbnail_id()) . "');\"" : '';
        ?>				
	<header class="article-header <?php echo $hclass ?>" <?php echo $hstyle ?> >	
        
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
		<p class="byline">
			Posted on <?php the_time('F j, Y') ?>
		</p>
    </header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('large'); ?>
		<?php the_content(); ?>
	</section> <!-- end article section -->
						
    <?php get_template_part( 'parts/content', 'sponsors' ); ?>
    
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
									
	<?php comments_template(); ?>	
													
</article> <!-- end article -->