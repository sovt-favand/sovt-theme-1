<?php
        $sports = get_field('sports_offered');
        /*echo '<pre>';
        var_dump($sports);
        echo '</pre>'; */
        if( $sports ): ?>
        
    <section class="sports">
        
            
                <h4>Sport<?php if (count($sports)>1): ?>s<?php endif; ?></h4>
                <div class="row ">
                
                <?php foreach( $sports as $sport):?>
                    
                    <div class="small-4 medium-3 large-2 column text-center <?php if($sport === end($sports)) echo "end" ?>">
                       <a href="<?php echo get_permalink($sport); ?>">
                                <?php echo get_the_post_thumbnail($sport, 'thumbnail'); ?>
                            
                       
                       <br>
                       <?php echo get_the_title($sport); ?>
                       </a>
                        
                    </div>
                        
                <?php endforeach; ?>
                </div>
            
            
        
    </section>
<?php endif; ?>