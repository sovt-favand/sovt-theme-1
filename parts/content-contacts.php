<?php
        $contacts = get_field('contacts');
        if( $contacts ): ?>
        
    <section class="contacts">
        
            
                <h4>Contact<?php if (count($contacts)>1): ?>s<?php endif; ?></h4>
                <div class="row ">
                
                <?php foreach( $contacts as $contact):?>
                    
                    <div class="media-object medium-6 large-4 column <?php if($contact === end($contacts)) echo "end" ?>">
                        <?php $contact_photo = get_post_meta($contact, 'photo', true);
                        if ($contact_photo): ?>
                            <div class="media-object-section">
                                <?php echo wp_get_attachment_image($contact_photo, 'thumbnail'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="media-object-section">
                            <h5><?php echo get_the_title($contact) ?></h5>
                            <p>
                                <?php echo antispambot(get_post_meta($contact, '_OrganizerEmail', true), 0) ?><br>
                                <?php echo get_post_meta($contact, '_OrganizerPhone', true) ?>
                            </p>
                        </div>
                    </div>
                        
                <?php endforeach; ?>
                </div>
            
            
        
    </section>
<?php endif; ?>