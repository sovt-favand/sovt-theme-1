<?php
$major_sponsors = get_field('major_sponsors');
$standard_sponsors = get_field('standard_sponsors');
$basic_sponsors = get_field('basic_sponsors');
        
if ($major_sponsors || $standard_sponsors || $basic_sponsors): ?>

    <h3 class="widgettitle">Sponsored by</h3>
        
    <?php if( $major_sponsors ): ?>
    
        <section class="row column mjr_spnsrs">
    
            
            
            <?php foreach( $major_sponsors as $major_sponsor):?>
                
                <div class="mjr_spnsr spnsr">
                <a href="<?php echo get_post_meta($major_sponsor, 'url', true) ?>">
                            <?php echo get_the_post_thumbnail($major_sponsor, 'medium'); ?>
                </a>
                    
                </div>
                    
            <?php endforeach; ?>
                
    
        </section>
        
    <?php endif; // major sponsors ?>

    <?php if( $standard_sponsors ): ?>
        
        <section class="row column stndrd_spnsrs">
            
            <?php foreach( $standard_sponsors as $standard_sponsor):?>
                
                <div class="stndrd_spnsr spnsr">
                    <a href="<?php echo get_post_meta($standard_sponsor, 'url', true) ?>">
                            <?php echo get_the_post_thumbnail($standard_sponsor, 'medium'); ?>
                    </a>
                    
                </div>
                    
            <?php endforeach; ?>
            

        </section>
       
    <?php endif; // standard sponsors ?>

    <?php if( $basic_sponsors ): ?>
        
        <section class="row column basic_spnsrs">
       
            
            
            <?php foreach( $basic_sponsors as $basic_sponsor):?>
                
                <div class="basic_spnsr spnsr">
                    <a href="<?php echo get_post_meta($basic_sponsor, 'url', true) ?>">
                            <b><?php echo get_the_title($basic_sponsor)  ?></b>
                    </a>
                    
                </div>
                    
            <?php endforeach; ?>
                  
    
        </section>
    <?php endif; // basic sponsors ?>
     <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('.mjr_spnsrs').owlCarousel({
                items: 3,
                itemsDesktop: false,
                itemsDesktopSmall: [1024, 3],
                itemsTablet: false,
                itemsMobile: [512, 1],
                autoPlay: 3000,
                
            });
            jQuery('.stndrd_spnsrs').owlCarousel({
                items: 4,
                itemsDesktop: false,
                itemsDesktopSmall: [1024, 4],
                itemsTablet: false,
                itemsMobile: [512, 2],
                autoPlay: 3000,
            });
        });
        </script>
<?php endif; //any sponsors ?>
