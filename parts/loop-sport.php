<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	<a class="previous-page-link" href="<?php echo get_post_type_archive_link('sport'); ?>">« All Sports</a>
        
	<header class="article-header">	
		<h1 class="entry-title single-title" itemprop="headline"><?php the_post_thumbnail('thumbnail'); ?> <?php the_title(); ?></h1>
	</header> <!-- end article header -->
	
    <?php // get_template_part( 'parts/content', 'contacts' ); ?>
    <?php // get_template_part( 'parts/content', 'sports' ); ?>
    
    <section class="entry-content" itemprop="articleBody">
		
		<?php the_content(); ?>
        
     </section>
     
     <!-- end article section -->
     
     <!-- begin upcoming competitions -->
     <?php
     $events = tribe_get_events( array(
        'start_date'     => date( 'Y-m-d H:i:s', strtotime( '-1 year' ) ),
        'eventDisplay'   => 'custom',
        'posts_per_page' => 5,
        'meta_query' => array(
                array(
                    'key' => 'sports_offered', // name of custom field
                    'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                    'compare' => 'LIKE'
                )
            ),
    ));
    
    ?>
    
    
     <?php if( $events ): ?>
            <h2>Recent and upcoming <?php the_title(); ?> events:</h2>
            <ul>
            <?php foreach( $events as $event ): ?>
                <?php

                $eventDate = date_create($event->EventStartDate);
                

                ?>
                 <li>
                    <a href="<?php echo get_permalink( $event->ID ); ?>">
                        <?php echo get_the_title( $event->ID ); ?>, <?php echo date_format($eventDate, 'F j, Y'); ?> 
                    </a>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php endif; ?>
     <!-- end upcoming competitions -->
     
     
     <!-- begin sport in-your-town list -->
        <?php
        $locals = get_posts(array(
            'post_type' => 'local',
            'meta_query' => array(
                array(
                    'key' => 'sports_offered', // name of custom field
                    'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                    'compare' => 'LIKE'
                )
            ),
            'order_by' => 'title'
        ));

        ?>
        
        <?php if( $locals ): ?>
            <h2><?php the_title(); ?> In Your Town:</h2>
            <ul>
            <?php foreach( $locals as $local ): ?>
                 <li>
                    <a href="<?php echo get_permalink( $local->ID ); ?>">
                        <?php echo get_the_title( $local->ID ); ?>
                    </a>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php endif; ?>
     <!-- end sport in-your-town --> 
              
	 
	
    <?php get_template_part( 'parts/content', 'sponsors' ); ?>
    					
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
									
	<?php comments_template(); ?>	
													
</article> <!-- end article -->