<?php get_header(); ?>
			
	<div id="content">
	
		<div id="inner-content" class="row">
		
		    <main id="main" class="large-12 medium-12 columns" role="main">
			    
		    	<header>
		    		<h1 class="page-title">Sports</h1>
					<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
		    	</header>
		
                <div class="row" data-equalizer>
		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
                    
             
					<?php
                    // TODO: abstract this out so it can also be used for program sport lists
                    ?>

                   

                            <!--Item: -->
                            <div class="small-6 medium-4 large-3 columns sport-panel ">
                            
                                <article data-equalizer-watch id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
                                
                                    <section class="featured-image" itemprop="articleBody">
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_post_thumbnail('medium'); ?>
                                        </a>
                                    </section> <!-- end article section -->
                                
                                    <header class="article-header text-center">
                                        <h3 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>	
                                       		
                                    </header> <!-- end article header -->	
                                                                                    
                                </article> <!-- end article -->
                                
                            </div>

                   
				    
				<?php endwhile; ?>	
                </div>

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
		
			</main> <!-- end #main -->
	
			<?php // get_sidebar(); ?>
	    
	    </div> <!-- end #inner-content -->
	    
	</div> <!-- end #content -->

<?php get_footer(); ?>